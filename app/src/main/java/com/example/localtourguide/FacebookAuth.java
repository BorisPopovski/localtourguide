package com.example.localtourguide;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Timer;
import java.util.TimerTask;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;


public class FacebookAuth extends AppCompatActivity {

    private LoginButton fbLogin;
    private static final String TAG = "FacebookAuthentication";
    private FirebaseAuth.AuthStateListener authStateListener;
    private AccessTokenTracker accessTokenTracker;
    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    private TextView textViewUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_facebook_auth);
       // AppEventsLogger.activateApp(this);

        mAuth = FirebaseAuth.getInstance();


        fbLogin=(LoginButton)findViewById(R.id.login_button);
        fbLogin.setReadPermissions("email");

        mCallbackManager = CallbackManager.Factory.create();

        textViewUser = findViewById(R.id.tvUser);

        fbLogin.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG,"onSuccess" + loginResult);
                handleFacebookToken(loginResult.getAccessToken());
//                startActivity(new Intent(FacebookAuth.this,SecondFragment.class));
//                finish();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "onCancel");
            }


            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "onError" + error);
            }
        });

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user=firebaseAuth.getCurrentUser();
                if(user!=null)
                {
                    updateUI(user);
                }
                else updateUI(null);
            }
        };

        accessTokenTracker = new AccessTokenTracker() {

            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if(currentAccessToken == null)
                {
                    mAuth.signOut();
                }
            }
        };

    }
        private void handleFacebookToken(AccessToken token) {

        Log.d(TAG,"handleFacebookToken" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful())
                {
                    Log.d(TAG,"sign in with credential: successful");
                    FirebaseUser user= mAuth.getCurrentUser();
                    updateUI(user);
                    startActivity(new Intent(FacebookAuth.this,MainActivity.class));
                    finish();
                }
                else{
                    Log.d(TAG,"Sign in with credentials: failed",task.getException());
                    Toast.makeText(FacebookAuth.this, "Authentication failed", Toast.LENGTH_SHORT).show();
                    updateUI(null);
                }
            }
        });
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, @Nullable Intent data)
    {
        mCallbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode,resultCode,data);
    }

    private void updateUI(FirebaseUser user) {
        if(user!=null)
        {
            textViewUser.setText(user.getDisplayName());
        }
        else textViewUser.setText("");
    }

}