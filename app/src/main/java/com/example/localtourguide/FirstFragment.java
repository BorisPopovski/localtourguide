package com.example.localtourguide;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.localtourguide.model.Guide;
import com.example.localtourguide.model.User;
import com.example.localtourguide.repository.GuideRepository;
import com.example.localtourguide.repository.UserRepository;

public class FirstFragment extends Fragment {
    private EditText etIndex, etName, etSurname, etPhone, etAddress;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_first, container, false);
        ConstraintLayout cl =(ConstraintLayout) inflater.inflate(R.layout.fragment_first,container,false);

        etIndex=cl.findViewById(R.id.etIndex);
        etName=cl.findViewById(R.id.etName);
        etSurname=cl.findViewById(R.id.etSurname);
        etPhone=cl.findViewById(R.id.etPhone);
        etAddress=cl.findViewById(R.id.etAddress);
        return cl;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // UserRepository userRepository=new UserRepository(getContext());
        GuideRepository guideRepository=new GuideRepository(getContext());
        view.findViewById(R.id.button_first).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });

        view.findViewById(R.id.btnBase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                NavHostFragment.findNavController(FirstFragment.this)
//                        .navigate(R.id.action_FirstFragment_to_SecondFragment);

                Guide guide = new Guide(etIndex.getText().toString(),
                        etName.getText().toString(),
                        etSurname.getText().toString(),
                        etAddress.getText().toString(),
                        etPhone.getText().toString());
                if(guide!=null){
                    guideRepository.addGuide(guide);
                }
                else{
                    Toast.makeText(getContext(),"Guide not found",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}