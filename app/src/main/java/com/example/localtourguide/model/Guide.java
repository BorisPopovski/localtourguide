package com.example.localtourguide.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Guide {
    @PrimaryKey
    @NonNull
   // @ColumnInfo(name="id")
    private String id;
   // @ColumnInfo(name="first_name")
    private String name;
   // @ColumnInfo(name="last_name")
    private String surname;
    private String phoneNumber;
    private String address;
//    private boolean isBooked;
//    private float price;
//
//    public boolean isBooked() {
//        return isBooked;
//    }
//
//    public void setBooked(boolean booked) {
//        isBooked = booked;
//    }
//
//    public float getPrice() {
//        return price;
//    }
//
//    public void setPrice(float price) {
//        this.price = price;
//    }

    public Guide() {
    }

    public Guide(String id, String name, String surname, String phoneNumber, String address) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
