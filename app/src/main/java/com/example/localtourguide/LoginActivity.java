package com.example.localtourguide;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Timer;
import java.util.TimerTask;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;


public class LoginActivity extends AppCompatActivity {

    private CallbackManager mCallbackManager;

    private FirebaseAuth mAuth;

    private EditText etMail, etPassword;
    private Button btnLogin, btnRegister;
    private TextView textViewUser;
    private LoginButton fbLogin;
    private static final String TAG = "FacebookAuthentication";
    private FirebaseAuth.AuthStateListener authStateListener;
    private AccessTokenTracker accessTokenTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        getSupportActionBar().hide();

//        new Timer().schedule(new TimerTask() {
//            @Override
//            public void run() {
//                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
//            }
//        },3000);






        etMail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        mAuth = FirebaseAuth.getInstance();

       // FacebookSdk.sdkInitialize(getApplicationContext());


//        fbLogin=findViewById(R.id.login_button);
//        fbLogin.setReadPermissions("email","public_profile");
//
//        mCallbackManager = CallbackManager.Factory.create();
//
//        fbLogin.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Log.d(TAG,"onSuccess" + loginResult);
//                handleFacebookToken(loginResult.getAccessToken());
//            }
//
//            @Override
//            public void onCancel() {
//                Log.d(TAG, "onCancel");
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Log.d(TAG, "onError" + error);
//            }
//        });



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etMail.getText().toString();
                String password = etPassword.getText().toString();

                if(email.isEmpty() || password.isEmpty())
                {
                    return;
                }
                login(email,password);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etMail.getText().toString();
                String password = etPassword.getText().toString();

                if(email.isEmpty() || password.isEmpty())
                {
                    return;
                }
                register(email,password);
            }
        });
//        authStateListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser user=firebaseAuth.getCurrentUser();
//                if(user!=null)
//                {
//                    updateUI(user);
//                }
//                else updateUI(null);
//            }
//        };

//        accessTokenTracker = new AccessTokenTracker() {
//            @Override
//            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
//                if(currentAccessToken == null)
//                {
//                    mAuth.signOut();
//                }
//            }
//        };
    }

//    private void handleFacebookToken(AccessToken token) {
//
//        Log.d(TAG,"handleFacebookToken" + token);
//
//        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
//        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//            @Override
//            public void onComplete(@NonNull Task<AuthResult> task) {
//                if(task.isSuccessful())
//                {
//                    Log.d(TAG,"sign in with credential: successful");
//                    FirebaseUser user= mAuth.getCurrentUser();
//                    updateUI(user);
//                }
//                else{
//                    Log.d(TAG,"Sign in with credentials: failed",task.getException());
//                    Toast.makeText(LoginActivity.this, "Authentication failed", Toast.LENGTH_SHORT).show();
//                    updateUI(null);
//                }
//            }
//        });
//    }

//    @Override
//    protected void onActivityResult (int requestCode, int resultCode, @Nullable Intent data)
//    {
//        mCallbackManager.onActivityResult(requestCode,resultCode,data);
//        super.onActivityResult(requestCode,resultCode,data);
//    }
//
//    private void updateUI(FirebaseUser user) {
//        if(user!=null)
//        {
//            textViewUser.setText(user.getDisplayName());
//        }
//        else textViewUser.setText("");
//    }

    private void register(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Successfully registered", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(LoginActivity.this, "Register failed", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void login(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            navigateLoggedInUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();


       // mAuth.addAuthStateListener(authStateListener);

        if(checkIfLoggedIn())
        {
             navigateLoggedInUser();
        }
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//
//        if(authStateListener!=null)
//        {
//            mAuth.removeAuthStateListener(authStateListener);
//        }
//    }

    private boolean checkIfLoggedIn()
    {
        return mAuth.getCurrentUser() != null;
    }

    private void navigateLoggedInUser()
    {
        startActivity(new Intent(LoginActivity.this,MainActivity.class));
        finish();
    }
}