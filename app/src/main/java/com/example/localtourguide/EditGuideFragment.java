package com.example.localtourguide;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.localtourguide.model.Guide;
import com.example.localtourguide.repository.GuideRepository;

public class EditGuideFragment extends Fragment {
    private GuideRepository guideRepository=new GuideRepository(getContext());
    private EditText etIndex,etName,etSurname,etPhone,etAddress;
    private Button btnEdit;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String guideID = getArguments().getString("guideID");
        Guide guide = guideRepository.findById(guideID);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        String guideID = getArguments().getString("guideID");
        Guide guide = guideRepository.findById(guideID);
        etIndex=view.findViewById(R.id.etIndex);
        etName=view.findViewById(R.id.etName);
        etSurname=view.findViewById(R.id.etSurname);
        etPhone=view.findViewById(R.id.etPhone);
        etAddress=view.findViewById(R.id.etAddress);
        etIndex.setText(guide.getId());
        etName.setText(guide.getName());
        etSurname.setText(guide.getSurname());
        etPhone.setText(guide.getPhoneNumber());
        etAddress.setText(guide.getAddress());

        Button edit = view.findViewById(R.id.btnEditData);
        edit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view) {
                Guide updateGuide=new Guide();
                updateGuide.setId(etIndex.getText().toString());
                updateGuide.setName(etName.getText().toString());
                updateGuide.setSurname(etSurname.getText().toString());
                updateGuide.setPhoneNumber(etPhone.getText().toString());
                updateGuide.setAddress(etAddress.getText().toString());

                guideRepository.updateGuide(updateGuide);

                NavController navController= Navigation.findNavController(view);
                navController.navigate(R.id.action_editGuideFragment_to_SecondFragment);

            }
        });
        Button back=view.findViewById(R.id.btnBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController=Navigation.findNavController(view);
                navController.navigate(R.id.action_editGuideFragment_to_FirstFragment);
            }
        });


    }
}
