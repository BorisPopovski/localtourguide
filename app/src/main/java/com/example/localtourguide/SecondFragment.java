package com.example.localtourguide;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.localtourguide.adapters.GuidesAdapter;
import com.example.localtourguide.repository.GuideRepository;
import com.example.localtourguide.utils.Constants;

public class SecondFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GuideRepository guideRepository=new GuideRepository(getContext());
        GuidesAdapter adapter=new GuidesAdapter(guideRepository.getAllGuides());
        RecyclerView recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setAdapter(adapter);

        adapter.setGuideClickListener(new GuidesAdapter.GuideClickListener() {
            @Override
            public void onGuideClicked(int position) {
                Bundle bundle=new Bundle();
                bundle.putInt(Constants.GUIDE_ARG_KEY, position);
                NavHostFragment.findNavController( SecondFragment.this).navigate(R.id.action_FirstFragment_to_SecondFragment,bundle);
            }


        });

//        Button book=view.findViewById(R.id.btnAdd);
//        book.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                NavController navController= Navigation.findNavController(view);
//                navController.navigate(R.id.action_SecondFragment_to_bookedGuide);
//            }
//        });

        recyclerView.setAdapter(adapter);
    }
}