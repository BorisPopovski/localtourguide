package com.example.localtourguide.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.localtourguide.R;
import com.example.localtourguide.model.Guide;
import com.example.localtourguide.repository.GuideRepository;

import java.util.List;

public class GuidesAdapter extends RecyclerView.Adapter<GuidesAdapter.GuideViewHolder> {

    private List<Guide> guides;
    private GuideClickListener guideClickListener;
    private Context context;
    private GuideRepository guideRepository = new GuideRepository(context);


    public GuidesAdapter(List<Guide> guides) {
        this.guides=guides;
    }

    @NonNull
    @Override
    public GuideViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row,null);
        return new GuideViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GuideViewHolder holder, int position) {
        holder.bindHolder(guides.get(position),guideClickListener);
        Button delete=holder.view.findViewById(R.id.btnDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guideRepository.deleteGuide(guides.get(position));
            }
        });
        Button edit=holder.view.findViewById(R.id.btnEdit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = Navigation.findNavController(view);
                Bundle bundle=new Bundle();
                bundle.putString("guideID",guides.get(position).getId());

                navController.navigate(R.id.action_SecondFragment_to_editGuideFragment,bundle);
            }
        });

        Button add=holder.view.findViewById(R.id.btnAdd);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = Navigation.findNavController(view);
                Bundle bundle=new Bundle();
                bundle.putString("guideID",guides.get(position).getId());

                navController.navigate(R.id.action_SecondFragment_to_bookedGuide,bundle);
            }
        });
    }

//    @Override
//    public void onBin c dViewHolder(@NonNull GuidesAdapter holder, int position) {
//
//    }

    @Override
    public int getItemCount() {
        return guides.size();
    }

    static class GuideViewHolder extends RecyclerView.ViewHolder {
        private TextView id,name,surname,phone,address;
        private View view;
        public GuideViewHolder(@NonNull View itemView) {
            super(itemView);
            view=itemView;
            id = itemView.findViewById(R.id.tvID);
            name = itemView.findViewById(R.id.tvName);
            surname = itemView.findViewById(R.id.tvSurname);
            phone = itemView.findViewById(R.id.tvPhone);
            address = itemView.findViewById(R.id.tvAddress);
        }
        public void bindHolder(final Guide guide, final GuideClickListener listener)
        {
            id.setText(guide.getId());
            name.setText(guide.getName());
            surname.setText(guide.getSurname());
            phone.setText(guide.getPhoneNumber());
            address.setText(guide.getAddress());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onGuideClicked(getAdapterPosition());
                }
            });
        }
    }

    public interface GuideClickListener{
        void onGuideClicked(int position);
    }

    public void setGuideClickListener(GuideClickListener guideClickListener)
    {
        this.guideClickListener=guideClickListener;
    }
}
