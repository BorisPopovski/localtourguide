package com.example.localtourguide.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.localtourguide.dao.GuideDao;
import com.example.localtourguide.dao.UserDao;
import com.example.localtourguide.model.Guide;
import com.example.localtourguide.model.User;

@Database(entities = {User.class, Guide.class} , version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract GuideDao guideDao();

    private static volatile AppDatabase INSTANCE;
    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "LocalTourGuide_database").allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }
}
