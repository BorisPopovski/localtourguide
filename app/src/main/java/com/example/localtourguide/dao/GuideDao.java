package com.example.localtourguide.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.localtourguide.model.Guide;


import java.util.List;

@Dao
public interface GuideDao {
    @Transaction
    @Query("SELECT * FROM Guide")
    public List<Guide> getAllGuides();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void addGuide(Guide guide);

    @Delete()
    public void deleteGuide(Guide guide);

    @Query("SELECT * FROM Guide WHERE `id` LIKE :id")
    public Guide findGuideByID (String id);

    @Update()
    public void updateGuide(Guide guide);

}
