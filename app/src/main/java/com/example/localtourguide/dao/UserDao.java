package com.example.localtourguide.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.localtourguide.model.User;

import java.util.List;

@Dao
public interface UserDao {
    @Transaction
    @Query("SELECT * FROM User")
    public List<User> getAllUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void addUser(User user);

    @Delete()
    public void deleteUser(User user);

    @Query("SELECT * FROM User WHERE `id` LIKE :id")
    public User findUserByID (String id);

    @Update()
    public void updateUser(User user);

}
