package com.example.localtourguide.repository;

import android.content.Context;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.localtourguide.dao.GuideDao;
import com.example.localtourguide.database.AppDatabase;
import com.example.localtourguide.model.Guide;

import java.util.List;

public class GuideRepository {
    private GuideDao guideDao;

    public GuideRepository(GuideDao guideDao){
        this.guideDao=guideDao;
    }

    public GuideRepository(Context context)
    {
        this.guideDao = AppDatabase.getDatabase(context).guideDao();
    }
    public List<Guide>getAllGuides(){return this.guideDao.getAllGuides();}
    public void addGuide(Guide guide){guideDao.addGuide(guide);}
    public void deleteGuide(Guide guide){guideDao.deleteGuide(guide);}
    public Guide findById(String id){return this.guideDao.findGuideByID(id);}
    public void updateGuide(Guide guide){guideDao.updateGuide(guide);}
}
