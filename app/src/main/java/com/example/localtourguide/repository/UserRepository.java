package com.example.localtourguide.repository;

import android.content.Context;

import com.example.localtourguide.dao.UserDao;
import com.example.localtourguide.database.AppDatabase;
import com.example.localtourguide.model.User;

import java.util.List;

public class UserRepository {
    private UserDao userDao;

    public UserRepository(UserDao userDao)
    {
        this.userDao=userDao;
    }

    public UserRepository(Context context){
        this.userDao = AppDatabase.getDatabase(context).userDao();
    }

    public List<User>getUsers(){return this.userDao.getAllUsers();}
    public void addUser(User user){userDao.addUser(user);}
    public void deleteUser(User user){userDao.deleteUser(user);}
    public User findUserByID(String id){return this.userDao.findUserByID(id);}
    public void updateUser(User user){userDao.updateUser(user);}

}
